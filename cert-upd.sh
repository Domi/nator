#!/bin/bash

function get_external_address() {
	local addr=$( timeout 3 dig +short myip.opendns.com @resolver1.opendns.com || \
	timeout 3 curl -s http://whatismyip.akamai.com/ || \
	timeout 3 curl -s http://ifconfig.io/ip || \
	timeout 3 curl -s http://ipecho.net/plain || \
	timeout 3 curl -s http://ident.me/
	)
	[ $? -ne 0 ] && addr="<this server IP address>"
	echo "$addr"
}

# args: file port password
function generate_config() {
# "fast_open": true reduces connection latency. But it doesn't work on OpenVZ, on old kernels, and on kernels where this feature is disabled
cat > "$1" <<EOF
{0
    
}
EOF
}



# args: port
function open_ufw_port() {
	# Open port in firewall if required
	if type ufw > /dev/null; then
	        ufw allow "$PORT"/tcp
	fi
}

# args: port
function open_firewalld_port() {
	# Open port in firewall if required
	if type firewall-cmd > /dev/null; then
		firewall-cmd --zone=public --permanent --add-port="$1"/tcp
		firewall-cmd --reload
	fi
}

# args: password port
function print_config() {
	echo
	echo "Your shadowsocks proxy configuration:"
	echo "URL: ss://$( generate_hash aes-256-gcm $1 )@$( get_external_address ):$2"
}

IFACE=$(ip route get 1.1.1.1 | head -1 | cut -d' ' -f5)
USER=user

[ -z "$PORT" ] && export PORT=8444
[ -z "$PASSWORD" ] && export PASSWORD=$( cat /dev/urandom | tr --delete --complement 'a-z0-9' | head --bytes=12 )

[ -e /etc/lsb-release ] && source /etc/lsb-release
[ -e /etc/os-release ] && source /etc/os-release


# Ubuntu 18.04 Bionic
if [ "$DISTRIB_ID $DISTRIB_CODENAME" = "Ubuntu bionic" ]; then

echo "--- OBNOVITES' DO Ubuntu 20.04 --- "

# Ubuntu 20.04 Focal
elif [ "$DISTRIB_ID $DISTRIB_CODENAME" = "Ubuntu focal" ]; then
sudo apt install snapd -y
sudo snap install core
sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

RED=`tput setaf 1`
GREEN=`tput setaf 2`
RESET=`tput sgr0`
echo
echo -e "${RED}===>>>Gotovo!${GREEN} Teper ustanovim Webinoly ${RESET}"

else

	echo "Skript podderjivaet tol'ko"
        echo "- Ubuntu 20.04 Focal"
	exit 1

fi